#!/usr/bin/env /bin/bash

dbhost=$(magerun db:info | grep 'host ' | cut -d '|' -f 3 | tr -d '[[:space:]]')
dbname=$(magerun db:info | grep 'dbname ' | cut -d '|' -f 3 | tr -d '[[:space:]]')
dbuser=$(magerun db:info | grep 'username ' | cut -d '|' -f 3 | tr -d '[[:space:]]')
dbpass=$(magerun db:info | grep 'password ' | cut -d '|' -f 3 | tr -d '[[:space:]]')

sed -i.bak 's/^\(host = \).*/\1"'"$dbhost"'"/' megatrixp/conf/magmi.ini
sed -i.bak 's/^\(dbname = \).*/\1"'"$dbname"'"/' megatrixp/conf/magmi.ini
sed -i.bak 's/^\(user = \).*/\1"'"$dbuser"'"/' megatrixp/conf/magmi.ini
sed -i.bak 's/^\(password = \).*/\1"'"$dbpass"'"/' megatrixp/conf/magmi.ini

