<?php
/**
 * Created by PhpStorm.
 * User: Hakum
 * Date: 20/10/17
 * Time: 12:03 AM
 */

if ( ! class_exists( 'MX' , FALSE ) ) {

    class MX
    {

        /** @var Megatrix_Core_Model_Miscellany_Log_Abstract[] */
        static private $loggers = array ();

        /**
         * @param string $which
         *
         * @return Megatrix_Core_Model_Miscellany_Log_Abstract
         */
        static public function getLogger ( $which = Megatrix_Core_Model_Miscellany_Log_General::class ) {

            if ( ! isset( static::$loggers[ $which ] ) ) {
                try {
                    $log = new $which();
                    static::$loggers[ $which ] = $log;
                } catch ( Exception $e ) {
                    static::getLogger()
                          ->alert( "$which is not a valid log class name" );
                }
            }

            $log = isset( static::$loggers[ $which ] ) ? static::$loggers[ $which ] : static::getLogger();

            return $log;
        }

        /**
         * @param mixed $mixed
         */

        static public function dump ( $mixed ) {

            MX::kint( $mixed , FALSE );
        }

        /**
         * @param string $p , ...
         *
         * @return string
         */
        static public function path ( $p ) {

            $arguments = func_get_args();

            return implode( DIRECTORY_SEPARATOR , $arguments );
        }

        /**
         * @param $mixed
         *
         * @return string|void
         */
        static public function kint ( $mixed , $cli = true ) {

            self::development();

            if ( ! $cli ) {
                Kint::$enabled_mode = Kint::$mode_default;
                Kint::dump( $mixed );
            }

            Kint::$enabled_mode = Kint::$mode_default_cli;
            return @Kint::dump( $mixed );
        }

        /**
         * @param $mixed
         *
         * @return string|void
         */
        static public function trace ( $mixed ) {

            self::development( FALSE );

            return @Kint::trace( $mixed );
        }

        static public function development () {

            $exists = @class_exists( 'Kint' );
            if ( ! $exists ) {
                /** @noinspection PhpIncludeInspection */
                include_once self::path( Mage::getBaseDir( 'lib' ) , 'Megatrix' , 'Kint' , 'kint-php' , 'kint' ,
                                         'build' ,
                                         'kint.php' );
                Kint::$display_called_from = FALSE;
            }

        }

        static public function getConfigData ( $path ) {

            return Mage::getStoreConfig( $path );
        }
    }
}
