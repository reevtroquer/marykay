<?php
/**
 * Created by PhpStorm.
 * User: Hakum
 * Date: 20/10/17
 * Time: 12:09 AM
 */

class Megatrix_Core_Model_Miscellany_PsrAutoload{

    private static $loaded = FALSE;

    public function __construct () {

        $this->fix();
    }

    private function fix () {

        if ( self::$loaded === FALSE ) {
            spl_autoload_register( array( $this, 'load' ), TRUE, TRUE );
        }
    }

    /**
     *   This function can autoloads classes starting with:
     *
     * - Symfony\Component\EventDispatcher
     * - Alterway\Component\Workflow
     * - EasyPost
     * - WsdlToPhp\PackageBase
     * - Sabre\Xml
     * - Sabre\Xml\Serializer
     * - Datapump\Product
     * - Datapump\Logger
     * - Datapump\Exception
     * - Go\Aop
     * - Go\Console
     * - Go\Core
     * - Go\Instrument
     * - Go\Lang
     * - Go\Proxy
     *
     * @param string $class
     */
    public function load ( $class ) {

        $match = preg_match(
            '#^(Kint\\\\Parser|Kint\\\\Object|Kint\\\\Renderer|Sabre\\\\Xml|Sabre\\\\Xml\\\\Serializer|Sabre\\\\Xml|Datapump\\\\Product|Datapump\\\\Logger|Datapump\\\\Exception|Go\\\\Aop|Go\\\\Console|Go\\\\Core|Go\\\\Instrument|Go\\\\Lang|Go\\\\Proxy)\b#',
            $class );
        if ( $match ) {

            $php_file =
                Mage::getBaseDir( 'lib' )
                . DIRECTORY_SEPARATOR
                .'Megatrix'
                . DIRECTORY_SEPARATOR
                . str_replace( '\\', DIRECTORY_SEPARATOR, $class ) . '.php';

            /** @noinspection PhpIncludeInspection */
            include_once( $php_file );

        } elseif ( $class == 'MX' ) {

            $lsr_file =
                Mage::getBaseDir( 'app' ) .
                DIRECTORY_SEPARATOR . 'code' .
                DIRECTORY_SEPARATOR . 'local' .
                DIRECTORY_SEPARATOR . 'Megatrix' .
                DIRECTORY_SEPARATOR . 'Core' .
                DIRECTORY_SEPARATOR . 'Model' .
                DIRECTORY_SEPARATOR . 'MX.php';

            /** @noinspection PhpIncludeInspection */
            include_once( $lsr_file );
        }
    }
}