<?php

class Megatrix_Core_Model_Miscellany_Log_General extends Megatrix_Core_Model_Miscellany_Log_Abstract
{

    const FILE = 'megatrix.log';

    /** @return string */
    function getFile () {
        return static::FILE;
    }
}
