<?php

abstract class Megatrix_Core_Model_Miscellany_Log_Abstract
{
    /** @var string */
    private $file = 'megatrix.log';


    /** @return string */
    abstract function getFile ();


    /** @param $file */
    public function setFile ( $file ) {
        $this->file = $file;
    }

    /**
     * @param string $message
     * @param int    $level
     */
    protected function log ( $message, $level = Zend_Log::DEBUG ) {

        Mage::log( $message, $level, $this->getFile() );
    }


    /** @param $message */
    public function alert ( $message ) {
        $this::log( $message, Zend_Log::ALERT );
    }

    /** @param $message */
    public function critical ( $message ) {
        $this::log( $message, Zend_Log::CRIT );
    }

    /** @param $message */
    public function debug ( $message ) {
        $this::log( $message, Zend_Log::DEBUG );
    }

    /** @param $message */
    public function error ( $message ) {
        $this::log( $message, Zend_Log::ERR );
    }

    /** @param $message */
    public function information ( $message ) {
        $this::log( $message, Zend_Log::INFO );
    }

    /** @param $message */
    public function notice ( $message ) {
        $this::log( $message, Zend_Log::NOTICE );
    }

    /** @param $message */
    public function warning ( $message ) {
        $this::log( $message, Zend_Log::WARN );
    }

    /** @param mixed $mixed , ... */
    public function dump ( $mixed , $cli = true ) {

        /** @noinspection PhpExpressionResultUnusedInspection */
        $arguments = func_get_args();
        foreach ( $arguments as $argument ) {
            $this->debug( MX::kint( $argument , $cli ) );
        }
    }
}
