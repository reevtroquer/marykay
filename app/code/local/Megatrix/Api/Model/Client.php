<?php

class Megatrix_Api_Model_Client extends Megatrix_Api_Model_AbstractClient
{
    const GET_TOKEN_AUTH_ENDPOINT = 'login/authenticate';
    const GET_CLIENTS_AUTH_ENDPOINT = 'programaLealtad/getClientes';

    public function getResponse()
    {
        return $this->getClientResponse()->getBody();
    }

    public function getAuthenticateToken()
    {
        $params = array(
            'userName' => 'Lealtad',
            'password' => '123456'
        );

        $this->setEndPoint(self::GET_TOKEN_AUTH_ENDPOINT);
        $this->setMethod(Zend_Http_Client::POST);
        $this->setParameters($params);

        return $this->getResponse();
    }

    public function getClients()
    {
        $headers = array(
            'Authorization' => 'Bearer ' . str_replace('"','', $this->getAuthenticateToken())
        );

        $this->setEndPoint(self::GET_CLIENTS_AUTH_ENDPOINT . '?paginaActual=1&registrosPorPagina=10');
        $this->setMethod(Zend_Http_Client::GET);
        $this->setHeaders($headers);

        return $this->getResponse();
    }
}