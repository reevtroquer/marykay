<?php

abstract class Megatrix_Api_Model_AbstractClient extends Mage_Core_Model_Abstract
{
    public $endPoint = null;
    public $method = null;
    public $headers = null;
    public $parameters = null;

    public function getClientUrl()
    {
        return 'http://200.53.184.212:8081/Lealtad/api/';
    }

    public function setEndPoint($endPoint)
    {
        $this->endPoint = $endPoint;
    }

    public function setHeaders($headers)
    {
        $this->headers = $headers;
    }

    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
    }

    public function setMethod($method)
    {
        $this->method = $method;
    }

    public function getClientResponse()
    {
        $client = new Zend_Http_Client($this->getClientUrl() . $this->endPoint);
        // $client->setMethod($this->method);
        $client->resetParameters();
        $client->setEncType(Zend_Http_Client::ENC_URLENCODED);
        $client->setParameterPost($this->parameters);
        if ($this->headers) $client->setHeaders($this->headers);
        $response = $client->request($this->method);

        if($response->getStatus() != 200){
            throw new Exception($response->getBody());
        }

        return $response;
    }
}