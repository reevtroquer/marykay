<?php

/**
 * Created by PhpStorm.
 * User: Hakum
 * Date: 10/04/17
 * Time: 2:45 PM
 */
class Megatrix_Pi_Adminhtml_PiController extends Mage_Adminhtml_Controller_Action
{

    private $minimun_data = array (
        'sku' ,
        'description' ,
        'short_description' ,
        'name' ,
        'weight' ,
        'price' ,
        'tax_class_id' ,
        'qty' ,
    );

    public function indexAction () {

        $this->_title( $this->__( 'pi' ) )
             ->_title( $this->__( 'Product Importer' ) );
        $this->loadLayout();
        $this->_setActiveMenu( 'pi/pi' );
        $this->_addContent( $this->getLayout()
                                 ->createBlock( 'megatrix_pi/adminhtml_pi' ) );
        $this->renderLayout();
    }

    public function gridAction () {

        $this->loadLayout();
        $this->getResponse()
             ->setBody(
                 $this->getLayout()
                      ->createBlock( 'megatrix_pi/adminhtml_pi_grid' )
                      ->toHtml()
             );
    }

    /**
     * Edit action - shows the edit form
     */
    public function editAction () {

        $id = $this->getRequest()
                   ->getParam( 'id' );

        $piModel = Mage::getModel( 'megatrix_pi/file' )
                       ->load( $id );


        if ( $piModel->getId() || $id == 0 ) {
            Mage::register( 'file_data' , $piModel );
            $this->loadLayout();
            $this->getLayout()
                 ->getBlock( 'head' )
                 ->setCanLoadExtJs( true );
            $this->_addContent( $this->getLayout()
                                     ->createBlock( 'megatrix_pi/adminhtml_edit' ) )
                 ->_addLeft( $this->getLayout()
                                  ->createBlock( 'megatrix_pi/adminhtml_edit_tabs' ) );
            $this->renderLayout();
        } else {
            Mage::getSingleton( 'adminhtml/session' )
                ->addError( 'Does not exist' );
            $this->_redirect( '*/*/' );
        }
    }

    /**
     * Edit action - shows the edit form
     */
    public function newAction () {

        $this->loadLayout();
        $this->_addContent( $this->getLayout()
                                 ->createBlock( 'megatrix_pi/adminhtml_edit' ) )
             ->_addLeft( $this->getLayout()
                              ->createBlock( 'megatrix_pi/adminhtml_edit_tabs' ) );
        $this->renderLayout();
        //$this->_forward('edit');
    }

    public function saveAction () {

        if ( $data = $this->getRequest()
                          ->getPost()
        ) {

            if ( isset( $data[ 'file_id' ] ) && ! empty( $data[ 'file_id' ] ) ) {
                $fileModel = Mage::getModel( 'megatrix_pi/file' )
                                 ->load( $data[ 'file_id' ] );
                $fileModel->setData( $data );
                try {
                    $fileModel->save();
                } catch ( Exception $e ) {
                    Mage::log( $e->getMessage() );
                }
                $this->_redirect( '*/*/' );
            } else {
                unset( $data[ 'file_id' ] );
                $fileModel = Mage::getModel( 'megatrix_pi/file' );
                $filename = NULL;
                $fullName = NULL;

                //if (isset($data['path']) && $postData['image'] != '') {
                if ( isset( $_FILES[ 'path' ][ 'name' ] ) && $_FILES[ 'path' ][ 'name' ] != '' && isset( $_FILES[ 'path' ][ 'size' ] ) && $_FILES[ 'path' ][ 'size' ] != 0 ) {
                    $path = Mage::getBaseDir( 'var' ) . DS . 'import' . DS;

                    Mage::log( $path );
                    if ( ! is_dir( $path ) ) {
                        mkdir( $path , 0777 , true );
                    }

                    $filename = $data[ 'name' ];
                    $fullName = $path . $filename;

                    while ( file_exists( $fullName . '.csv' ) ) {
                        $pieces = array ();

                        $res = preg_match( '/^(.+)_(\d+)$/' , $fullName , $pieces );

                        if ( ! $res ) {
                            $fullName .= '_1';
                            $filename .= '_1';
                        } else {
                            $fullName .= '_' . strval( intval( $pieces[ 2 ] ) + 1 );
                            $filename .= '_' . strval( intval( $pieces[ 2 ] ) + 1 );
                        }
                    }

                    $fullName .= '.csv';
                    $filename .= '.csv';

                    try {
                        $uploader = new Varien_File_Uploader( 'path' );
                        $uploader->setAllowedExtensions( array ( 'csv' ) );
                        $uploader->setAllowRenameFiles( true );
                        $uploader->setFilesDispersion( false );
                        $uploader->save( $path , $filename );
                    } catch ( Exception $e ) {
                        Mage::log( $e->getMessage() );
                        Mage::getSingleton( 'core/session' )
                            ->addError( $e->getMessage() );
                        $this->_redirect( '*/*/' );
                        return;
                    }
                }

                $data[ 'path' ] = $fullName;
                $fileModel->setData( $data );
                try {
                    $fileModel->save();
                } catch ( Exception $e ) {
                    Mage::log( $e->getMessage() );
                }

                $this->_redirect( '*/*/' );
            }
        }
    }

    public function deleteAction () {

        if ( $id = $this->getRequest()
                        ->getParam( 'id' )
        ) {
            try {
                $fileModel = Mage::getModel( 'megatrix_pi/file' )
                                 ->load( $id );

                $path = $fileModel->getData( 'path' );
                $cmd = 'rm -f ' . $path;

                $fileModel->delete();

                Mage::getSingleton( 'adminhtml/session' )
                    ->addSuccess( Mage::helper( 'adminhtml' )
                                      ->__( 'Item was successfully deleted' ) );
                $this->_redirect( '*/*/' );
            } catch ( Exception $e ) {
                Mage::getSingleton( 'adminhtml/session' )
                    ->addError( $e->getMessage() );
                $this->_redirect( '*/*/edit' , array ( 'id' => $this->getRequest()
                                                                    ->getParam( 'id' ) ) );
            }
        }
        $this->_redirect( '*/*/' );
    }

    public function processFileAction () {

        $file = $this->getRequest()
                     ->getParam( 'url' );
        $session = Mage::getSingleton( 'adminhtml/session' );
        Mage::log( '***** Comienza Proceso del CSV ' . $file , null , 'Megatrix_Process_Website_Price.log' );
        if ( file_exists( $file ) ) {
            $helper = Mage::helper( 'megatrix_pi' );
            $csv = new Varien_File_Csv();
            $csvData = $csv->getData( $this->getRequest()
                                           ->getParam( 'url' ) );

            $columns = array_shift( $csvData );

            if ( ! in_array( 'sku' , $columns ) ) {
                $session->addError( 'El Archivo ' . basename( $this->getRequest()
                                                                   ->getParam( 'url' ) ) . ' no se proceso. No se encontro columna sku' );
                return;
            }

//            foreach ( $csvData as $k => $v ) {
//                $csvData[ $k ] = array_combine( $columns , array_values( $v ) );
//            }

            if ( ! $this->_isUpdate( $columns ) ) {
                $helper->importProducts( $this->getRequest()
                                              ->getParam( 'url' ) , $this->getRequest()
                                                                         ->getParam( 'id' ) );
            } else {
                $helper->updateProducts( $file , $this->getRequest()
                                                      ->getParam( 'id' ) );
            }


            $session->addSuccess( 'El Archivo ' . basename( $this->getRequest()
                                                                 ->getParam( 'url' ) ) . ' se proceso de forma correcta.' );
            return;
        }

        $session->addError( 'El Archivo ' . basename( $this->getRequest()
                                                           ->getParam( 'url' ) ) . ' no se proceso.' );
        return;
    }

    protected function _isAllowed () {

        return Mage::getSingleton( 'admin/session' )
                   ->isAllowed( 'admin/system/megatrix/pi' );
    }

    protected function _isUpdate ( $columns ) {

        $intersect = array_intersect( $this->minimun_data , $columns );

        if ( count( $intersect ) >= 7 ) {
            return FALSE;
        }

        return true;
    }

    public function logAction () {

        $params = $this->getRequest()
                       ->getParams();
        $file_id = $params[ 'file_id' ];

        Mage::getSingleton( 'adminhtml/session' )
            ->setData( 'file_id' , $file_id );

        $this->loadLayout()
             ->_title( $this->__( 'Log ' . $file_id ) );

        $this->renderLayout();
    }
}
