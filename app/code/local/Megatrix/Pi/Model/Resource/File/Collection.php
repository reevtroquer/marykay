<?php
/**
 * Created by PhpStorm.
 * User: Hakum
 * Date: 10/04/17
 * Time: 2:07 PM
 */

class Megatrix_Pi_Model_Resource_File_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract{

    public function _construct () {

        $this->_init('megatrix_pi/file');
    }
}