<?php
/**
 * Created by PhpStorm.
 * User: Hakum
 * Date: 10/04/17
 * Time: 2:05 PM
 */

class Megatrix_Pi_Model_Resource_File extends Mage_Core_Model_Resource_Db_Abstract{

    protected function _construct () {

       $this->_init('megatrix_pi/file','file_id');
    }
}