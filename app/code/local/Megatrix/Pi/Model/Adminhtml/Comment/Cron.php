<?php
/**
 * Created by PhpStorm.
 * User: Hakum
 * Date: 06/12/17
 * Time: 6:56 PM
 */

class Megatrix_Pi_Model_Adminhtml_Comment_Cron
{

    public function getCommentText () {
        return '<p>0m | 21hrs | All month | EveryMonths | EveryWeek</p>' .
            '<p>0 21 * * *</p>' .
            '<p><a href="http://cron.nmonitoring.com/cron-generator.html" target="_blank">Cron Helper</a></p>';
    }
}