<?php
/**
 * Created by PhpStorm.
 * User: Hakum
 * Date: 07/12/17
 * Time: 12:19 AM
 */

include '/var/www/html/app/code/local/Megatrix/Core/Model/MX.php';

class Megatrix_Pi_Model_Observer
{

    private $csv_new_fields = array (
        'sku' ,
        'attribute_set' ,
        'type' ,
        'store' ,
        'name' ,
        'description' ,
        'price' ,
        'qty' ,
        'is_in_stock' ,
        'manage_stock' ,
        'status' ,
        'visibility' ,
        'weight' ,
        'categories' ,
        'thumbnail' ,
        'small_image' ,
        'image' ,
        'media_gallery' ,
        'linio_id' ,
        'brand' ,
        'longdescription' ,
        'pricespecial' ,
        'discountvalue' ,
        'discountpercentage' ,
        'freeshipping' ,
        'color' ,
        'shipping_cost' ,
        'size' ,
        'is_imported' ,
        'shipping_estimate' ,
        'tax_class_id',
        'cost'
    );

    private $minimun_data = array (
        'sku' ,
        'description' ,
        'short_description' ,
        'name' ,
        'weight' ,
        'price' ,
        'tax_class_id' ,
        'qty' ,
    );

    protected function _get_linio_csv () {

        $url_feed = MX::getConfigData( 'appetite/linio/uri_feed' );
        $linio_feed_data = file_get_contents( $url_feed );

        MX::getLogger()->dump($linio_feed_data);

        $this->generate_csv_new( $linio_feed_data );

        return;
    }

    protected function _getCsvHeaders () {

        return $this->csv_new_fields;
    }

    public static function getParsedString($string)
    {
        $string = str_replace(
            array(
                "&aacute;","&eacute;","&iacute;","&oacute;","&uacute;","&ntilde;","&Aacute;","&Eacute;","&Iacute;","&Oacute;","&Uacute;","&Ntilde;",
                "&comma;","&gt;","&period;","&percnt;","&semi;","&colon;","&lpar;","&ast;","&sol;","&lowbar;","&rpar;","&amp;","nbsp%","bull%"
            ),
            array("á","é","í","ó","ú","ñ","Á","É","Í","Ó","Ú","Ñ",
                " "," "," "," ","%"," "," "," "," "," "," "," "," "," "," "
            ),
            $string
        );

        $string = preg_replace('/(\&.*?\;)/', ' ', $string);
        $string = trim(preg_replace('/\s+/', ' ', $string));

        return $string;
    }

    /**
     * @param $_data
     * @return array
     * @throws Exception
     */
    public function generate_csv_new ($_rows) {
        $io = new Varien_Io_File();
        $path = Mage::getBaseDir( 'var' ) . DS . MX::getConfigData( 'megatrix/pi/csv_path' );

        if ( ! is_dir( $path ) ) {
            mkdir( $path , 0777 , true );
        }

        $name = '1.new_' . md5( microtime() );
        $file = $path . DS . $name . '.csv';
        $io->setAllowCreateFolders( true );
        $io->open( array ( 'path' => $path ) );
        $io->streamOpen( $file , 'w+' );
        $io->streamLock( true );

        $io->streamWriteCsv( $this->_getCsvHeaders() );

        foreach ($_rows as $line) {
            $line = explode(';', $this->getParsedString(str_replace('"', '', $line[0])));

            if ($line[0] == 'id') {
                continue;
            }

            $categoria = '';
            $enStock = ($line[20] > 0) ? '1' : '0';
            if ((trim($line[5]) <> '') or (sizeof($line[5]) > 0)) {
                $categoria = trim($line[5]);
            }
            if ((trim($line[6]) <> '') or (!empty($line[6]))) {
                $categoria .= '/' . trim($line[6]);
            }
            if ((trim($line[7]) <> '') or (!empty($line[7]))) {
                $categoria .= '/' . trim($line[7]);
            }
            if ((trim($line[8]) <> '') or (!empty($line[8]))) {
                $categoria .= '/' . trim($line[8]);
            }

            MX::getLogger()->debug(trim($line[1]));
            $price = trim($line[15]);
            MX::getLogger()->debug('ORGINAL PRICE: ' . $price);

            $profit = Mage::getStoreConfig('appetite/linio/profit');
            if ($profit && $profit > 0) {
                MX::getLogger()->debug('PROFIT: ' . $profit);
                $aux = 1 + ($profit / 100);
                $price = trim($line[15]) * $aux;
            }


            MX::getLogger()->debug('NEW PRICE: ' . $price);

            $csv_content =
                array(
                    trim($line[1]), // sku
                    'Default', // attribute_set
                    'simple', // type
                    'admin', // store
                    trim($line[2]), // name
                    trim($line[12]), // description
                    $price, // price
                    trim($line[20]), // qty
                    $enStock, // is_in_stock
                    '1', // manage_stock
                    '1', // status
                    'Catalog, Search', // visibility
                    '0', // weight
                    $categoria, // categories
                    trim($line[9]), // thumbnail
                    trim($line[9]), // small_image
                    trim($line[9]), // image
                    trim($line[9]), // media_gallery
                    trim($line[0]), // linio_id
                    trim($line[3]), // brand
                    trim($line[13]), // longdescription
                    trim($line[16]), // pricespecial
                    trim($line[17]), // discountvalue
                    trim($line[18]), // discountpercentage
                    '0', // freeshipping
                    trim($line[26]), // color
                    '0', // shipping_cost
                    trim($line[30]), // size
                    trim($line[34]), // is_imported
                    '0', // shipping_estimate
                    2, // tax_class_id
                    $line[15] // cost
                );

            MX::getLogger()->dump($csv_content);

            $io->streamWriteCsv($csv_content);
        }

        return array (
            'type' => 'filename' ,
            'value' => $file ,
            'rm' => true // can delete file after use
        );
    }

    /**
     * @param Mage_Cron_Model_Schedule $event_observer
     * @throws Exception
     */
    public function massive_import (/*Mage_Cron_Model_Schedule $event_observer*/) {
        exec("wget -P /var/www/html http://feed.linio.com/mx/appetite.csv");

        if(($handle = fopen('/var/www/html/appetite.csv', 'r')) !== false)
        {
            $_rows = [];
            $_totalRows = 0;
            $_lines = 0;
            $header = fgetcsv($handle);

            while(($row = fgetcsv($handle)) !== false)
            {
                $_totalRows++;

                if ($_lines === 0){
                    $_rows[] = $header;
                }

                $_rows[] = $row;
                $_lines++;

                if ($_lines === 3000){
                    var_dump('Procesando bloque: ' . $_totalRows);
                    $this->massive_import_large($_rows);
                    $_rows = [];
                    $_lines = 0;
                }

                unset($row);
            }

            fclose($handle);
        }else{
            var_dump("Can't load!");
        }

        exec("rm /var/www/html/appetite.csv");
    }

    /**
     * @param $_rows
     * @throws Exception
     */
    public function massive_import_large ($_rows) {

        MX::getLogger()
            ->debug( 'OBSERVER massive_import ===========' );

        $this->generate_csv_new($_rows);

        $helper = Mage::helper( 'megatrix_pi' );
        $dir_configured = Mage::getStoreConfig( 'megatrix/pi/csv_path' );
        $base = Mage::getBaseDir( 'var' );

        $full_path = $base . $dir_configured;

        $dir = dir( $full_path ); //Open Directory
        while ( false !== ( $file = $dir->read() ) ) //Reads Directory
        {
            $just_name = $file;
            $extension = substr( $file , strrpos( $file , '.' ) ); // Gets the File Extension
            $file = $full_path . $file;

            if ( $extension == ".csv" ) {
                $csv = new Varien_File_Csv();
                $csvData = $csv->getData( $file );
                $columns = array_shift( $csvData );

                if ( ! in_array( 'sku' , $columns ) ) {
                    MX::getLogger()
                        ->error( $file . ' no se proceso. No se encontro columna sku' );

                    if ( ! is_dir( $full_path . DS . 'Trash' ) ) {
                        mkdir( $full_path . DS . 'Trash' , 0777 , true );
                    }
                    $trash = $full_path . DS . 'Trash' . DS . $just_name;
                    rename( $file , $trash );
                    continue;
                }

                if ( ! $this->_isUpdate( $columns ) ) {
                    $helper->importProducts( $file , NULL );
                } else {
                    $helper->updateProducts( $file , NULL );
                }


                MX::getLogger()
                    ->debug( 'El Archivo ' . $file . ' se proceso de forma correcta.' );
                return;
            }
        }
        $dir->close(); // Close Directory
    }

    protected function _isUpdate ( $columns ) {

        $intersect = array_intersect( $this->minimun_data , $columns );

        if ( count( $intersect ) >= 7 ) {
            return FALSE;
        }

        return true;
    }

}
