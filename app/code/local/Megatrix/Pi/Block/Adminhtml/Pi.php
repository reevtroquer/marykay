<?php

/**
 * Created by PhpStorm.
 * User: Hakum
 * Date: 10/04/17
 * Time: 2:47 PM
 */
class Megatrix_Pi_Block_Adminhtml_Pi extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct () {

        $this->_blockGroup = 'megatrix_pi';
        $this->_controller = 'adminhtml_pi';
        $url = Mage::getUrl( 'megatrixp/web' );
        $url = str_replace( 'admin/index.php' , '' , $url );
        $url = str_replace( '/index.php' , '' , $url );
        $url = str_replace( '/admin/' , '/' , $url );
        $url .= 'magmi.php';
        $this->_headerText = Mage::helper( 'megatrix_pi' )
                                 ->__( 'Despues de subir el archivo pulsa procesar.');
        parent::__construct();
    }
}