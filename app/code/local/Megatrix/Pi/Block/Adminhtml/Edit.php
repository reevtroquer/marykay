<?php
/**
 * Created by PhpStorm.
 * User: Hakum
 * Date: 10/04/17
 * Time: 3:16 PM
 */

class Megatrix_Pi_Block_Adminhtml_Edit extends Mage_Adminhtml_Block_Widget_Form_Container{

    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'id';
        //you will notice that assigns the same blockGroup the Grid Container
        $this->_blockGroup = 'megatrix_pi';
        // and the same container
        $this->_controller = 'adminhtml';
        $this->_mode = 'edit';
        //we define the labels for the buttons save and delete
        $this->_updateButton('save', 'label','Salvar');
        $this->_updateButton('delete', 'label', 'Borrar');
    }

    public function getHeaderText()
    {
        if( Mage::registry('megatrix_file') && Mage::registry('megatrix_file')->getId() )
        {
            return 'Editar '.$this->htmlEscape( Mage::registry('megatrix_file')->getName() );
        }
        else
        {
            return 'Añadir';
        }
    }
}