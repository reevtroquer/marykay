<?php
/**
 * Created by PhpStorm.
 * User: Hakum
 * Date: 10/04/17
 * Time: 3:36 PM
 */

class Megatrix_Pi_Block_Adminhtml_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form{

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();


        $fieldset = $form->addFieldset('form_form',
                                       array('legend'=>Mage::helper('megatrix_pi')->__('File information')));

        $fieldset->addField('file_id', 'hidden', array(
            'label'     => Mage::helper('megatrix_pi')->__('Id'),
            'name'      => 'file_id',
        ));

        $fieldset->addField('name', 'text', array(
            'label'     => Mage::helper('megatrix_pi')->__('Nombre'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'name',
        ));

        $fieldset->addField('path', 'file', array(
            'label'     => Mage::helper('megatrix_pi')->__('File'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'path',
        ));

        $this->setForm($form);
        if ( Mage::registry('file_data') ) {
            $values = Mage::registry('file_data')->getData();
            Mage::log($values);
            $form->setValues($values);
        }

        return parent::_prepareForm();
    }
}