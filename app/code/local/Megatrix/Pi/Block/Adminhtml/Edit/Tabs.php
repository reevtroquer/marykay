<?php
/**
 * Created by PhpStorm.
 * User: Hakum
 * Date: 10/04/17
 * Time: 3:33 PM
 */

class Megatrix_Pi_Block_Adminhtml_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs{

    public function __construct()
    {
        parent::__construct();
        $this->setId('form_tabs');
        $this->setDestElementId('edit_form'); // this should be same as the form id define above
        $this->setTitle(Mage::helper('megatrix_pi')->__('File Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('megatrix_pi')->__('File Information'),
            'title'     => Mage::helper('megatrix_pi')->__('File Information'),
            'content'   => $this->getLayout()->createBlock('megatrix_pi/adminhtml_edit_tab_form')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}