<?php
/**
 * Created by PhpStorm.
 * User: Hakum
 * Date: 10/04/17
 * Time: 2:48 PM
 */

class Megatrix_Pi_Block_Adminhtml_Pi_Grid extends Mage_Adminhtml_Block_Widget_Grid{

    public function __construct()
    {
        parent::__construct();
        $this->setId('megatrix_pi_grid');
        $this->setDefaultSort('file_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('megatrix_pi/file')->getCollection();

        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }

    protected function _prepareColumns()
    {
        $helper = Mage::helper('megatrix_pi');

        $this->addColumn('file_id', array(
            'header' => $helper->__('File #'),
            'index'  => 'file_id'
        ));

        $this->addColumn('name', array(
            'header' => $helper->__('Name'),
            'index'  => 'name'
        ));

        $this->addColumn('path', array(
            'header' => $helper->__('Path'),
            'index'  => 'path',
            'actions'   => false,
        ));
        
        $this->addColumn('process', array(
            'header' => Mage::helper('megatrix_pi')->__('Procesar'),
            'align'  => 'center',
            'filter' => false,
            'renderer' => 'megatrix_pi/adminhtml_widget_grid_column_renderer_inline',
            'width' =>'390px',
        ));

        $this->addColumn('log',
                         array(
                             'header'    => Mage::helper('sales')->__('Action'),
                             'width'     => '60px',
                             'type'      => 'action',
                             'getter'     => 'getFileId',
                             'actions'   => array(
                                 array(
                                     'caption' => Mage::helper('sales')->__('Log'),
                                     'url'     => array('base'=>'*/*/log'),
                                     'field'   => 'file_id',
                                     'onclick' => 'javascript:logPopup(this)'
                                 )
                             ),
                             'filter'    => false,
                             'sortable'  => false,
                             'index'     => 'log',
                             'is_system' => true,
                         ));
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    //public function getRowUrl ( $row ) {

        //return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    //}
}