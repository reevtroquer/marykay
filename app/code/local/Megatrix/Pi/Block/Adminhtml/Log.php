<?php
/**
 * Created by PhpStorm.
 * User: Hakum
 * Date: 06/12/17
 * Time: 9:27 PM
 */

class Megatrix_Pi_Block_Adminhtml_Log extends Mage_Adminhtml_Block_Template
{

    public function getLog () {

        $session = Mage::getSingleton( 'adminhtml/session' );
        $file_id = $session->getData( 'file_id' );

        $fileModel = Mage::getModel( 'megatrix_pi/file' )
                         ->load( $file_id );

        if($fileModel->getId()){
            return unserialize($fileModel->getData('log'));
        }
        return FALSE;
    }

}