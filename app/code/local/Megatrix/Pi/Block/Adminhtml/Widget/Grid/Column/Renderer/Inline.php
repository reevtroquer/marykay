<?php
/*
 * @author UrielHR
 * Created By NetBeans
 */
class Megatrix_Pi_Block_Adminhtml_Widget_Grid_Column_Renderer_Inline extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Text {

    public function render(Varien_Object $row) {
        $html = parent::render($row);
        $urlFile = $row->getPath();
        if($row->getProcess() == 0){
            $html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span javascript:void(0); id="process-'.$row->getId().'" style="background-color: red;" '
                . ' onclick="updateField(this, ' . $row->getId(). '); "><strong><font face="arial" size=4>' . Mage::helper('megatrix_pi')->__('Procesar') . '</font></strong></span>';
            $html .= '<input id="path_file" type="hidden" value="'.$urlFile.'">';
        }else{
            $html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="process-'.$row->getId().'" style="background-color: #64FE2E;"><strong><font face="arial" size=4>' . Mage::helper('megatrix_pi')->__('Procesado') . '</font></strong></span>';
            $html .= '<input id="path_file" type="hidden" value="'.$urlFile.'">';
        }
        
        return $html;
    }

}
