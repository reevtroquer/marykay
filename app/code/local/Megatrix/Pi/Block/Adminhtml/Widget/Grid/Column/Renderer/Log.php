<?php

/*
 * @author UrielHR
 * Created By NetBeans
 */

class Megatrix_Pi_Block_Adminhtml_Widget_Grid_Column_Renderer_Log
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Text
{

    public function render ( Varien_Object $row ) {

        $html = parent::render( $row );
        $urlFile = $row->getPath();
        $html .= '<span javascript:void(0); id="log-' . $row->getId() . ' onclick="showLog(this, ' .
            $row->getId() . '); "><strong><font face="arial" size=4>' . Mage::helper( 'megatrix_pi' )
                                                                            ->__( 'Log' ) . '</font></strong></span>';
        $html .= '<input id="path_file" type="hidden" value="' . $urlFile . '">';

        return $html;
    }

}
