<?php
/**
 * Created by PhpStorm.
 * User: Hakum
 * Date: 10/04/17
 * Time: 1:32 PM
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()
                   ->newTable( $installer->getTable( 'megatrix_pi/file' ) )
                   ->addColumn( 'file_id' , Varien_Db_Ddl_Table::TYPE_INTEGER , null , array (
                       'identity' => true ,
                       'unsigned' => true ,
                       'nullable' => false ,
                       'primary' => true ,
                   ) , 'Id' )
                   ->addColumn( 'name' , Varien_Db_Ddl_Table::TYPE_VARCHAR , null , array (
                       'nullable' => false ,
                   ) , 'Title' )
                   ->addColumn( 'path' , Varien_Db_Ddl_Table::TYPE_TEXT , null , array (
                       'nullable' => false ,
                   ) , 'path' );
$installer->getConnection()
          ->createTable( $table );

$installer->endSetup();