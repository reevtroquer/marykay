<?php
/**
 * Created by PhpStorm.
 * User: Hakum
 * Date: 10/04/17
 * Time: 1:32 PM
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()
          ->addColumn(
              $this->getTable( 'megatrix_pi/file' ) , 'city' , array (
                                                             'type' => Varien_Db_Ddl_Table::TYPE_TEXT ,
                                                             'nullable' => true ,
                                                             'length' => 255 ,
                                                             'comment' => 'City'
                                                         )
          );

$installer->getConnection()
          ->addColumn(
              $this->getTable( 'megatrix_pi/file' ) , 'region' , array (
                                                             'type' => Varien_Db_Ddl_Table::TYPE_TEXT ,
                                                             'nullable' => true ,
                                                             'length' => 255 ,
                                                             'comment' => 'State'
                                                         )
          );

$installer->getConnection()
          ->addColumn(
              $this->getTable( 'megatrix_pi/file' ) , 'postcode' , array (
                                                             'type' => Varien_Db_Ddl_Table::TYPE_TEXT ,
                                                             'nullable' => true ,
                                                             'length' => 255 ,
                                                             'comment' => 'Cp'
                                                         )
          );

$installer->endSetup();