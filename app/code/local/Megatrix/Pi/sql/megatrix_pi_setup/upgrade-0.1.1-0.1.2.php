<?php

/**
 * Created by NetBeans.
 * User: UrielHR
 */
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
        ALTER TABLE megatrix_file ADD process INT(2) NOT NULL DEFAULT 0;
SQLTEXT;
$installer->run($sql);
$installer->endSetup();
	 