<?php
/**
 * Created by PhpStorm.
 * User: Hakum
 * Date: 10/04/17
 * Time: 1:32 PM
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()
          ->dropColumn(
              $this->getTable( 'megatrix_pi/file' ) , 'log' );

$installer->getConnection()
          ->addColumn(
              $this->getTable( 'megatrix_pi/file' ) , 'log' , array (
                                                             'type' => Varien_Db_Ddl_Table::TYPE_TEXT ,
                                                             'nullable' => true ,
                                                             'length' => Varien_Db_Ddl_Table::DEFAULT_TEXT_SIZE ,
                                                             'comment' => 'log'
                                                         )
          );


$installer->endSetup();