<?php

/**
 * Created by PhpStorm.
 * User: Hakum
 * Date: 10/04/17
 * Time: 1:32 PM
 */
class Megatrix_Pi_Helper_Data extends Mage_Core_Helper_Abstract
{

    private $store_code = 0;

    public function updateProductsBAD ( $csv, $file , $fileId ) {

        $log = '';
        $session = Mage::getSingleton( 'adminhtml/session' );
        foreach ( $csv as $row ) {
            if ( $product = $this->_existProduct( $row[ 'sku' ] ) ) {
                $attrData = array ();
                foreach ( $row as $attribute => $value ) {
                    if ( $attribute == 'sku' ) {
                        continue;
                    }

                    switch ( $attribute ) {
                        case 'store' :
                            $this->_updateWbs( $product , $value );
                            break;
                        default :
                            $attrData[ $attribute ] = $value;
                            continue;
                            break;
                    }
                }

                if ( ! empty( $attrData ) ) {
                    $this->_updateAttr( $product , $attrData );
                }

            } else {
                $log .= '<p>' .  'SKU :' . $row[ 'sku' ] . 'no existe.' . '</p>';
                continue;
            }
        }

        $this->mvFile( $file , $fileId , $log , 'update');

        return;
    }

    public function updateProducts ( $file , $fileId ) {

        $magmi_base_dir = Mage::getBaseDir() . DS . 'megatrixp' . DS;
        $progress_file = $magmi_base_dir . 'state' . DS . 'progress.txt';
        unlink( $progress_file );
        MX::getLogger()
          ->debug( $magmi_base_dir );
        $cmd = 'php ' . $magmi_base_dir . 'cli' . DS . 'magmi.cli.php -mode=create -CSV:filename="' . $file . '"';
        MX::getLogger()
          ->debug( $cmd );
        $result = system( $cmd , $last );
        $log = file_get_contents( $progress_file );
        MX::getLogger()
          ->dump( 'ACABO' );
        MX::getLogger()
          ->dump( $log );

        $this->mvFile( $file , $fileId , $log , 'update');

        return;
    }

    public function importProducts ( $file , $fileId ) {

        $magmi_base_dir = Mage::getBaseDir() . DS . 'megatrixp' . DS;
        $progress_file = $magmi_base_dir . 'state' . DS . 'progress.txt';
        unlink( $progress_file );
        MX::getLogger()
          ->debug( $magmi_base_dir );
        $cmd = 'php ' . $magmi_base_dir . 'cli' . DS . 'magmi.cli.php -mode=create -CSV:filename="' . $file . '"';
        MX::getLogger()
          ->debug( $cmd );
        $result = system( $cmd , $last );
        $log = file_get_contents( $progress_file );
        MX::getLogger()
          ->dump( 'ACABO' );
        MX::getLogger()
          ->dump( $log );

        $this->mvFile( $file , $fileId , $log , 'import');

        return;
    }

    public function mvFile ( $file , $fileId , $log , $type ) {

        $data = array ();
        $fileModel = NULL;
        MX::getLogger()->dump($fileId);
        $dir_configured = Mage::getStoreConfig( 'megatrix/pi/csv_path' );
        $base = Mage::getBaseDir( 'var' );

        $data[ 'name' ] = $type . '_' . date( 'mdY-H:m' , strtotime( 'now' ) );
        $path = $base . $dir_configured . 'Prosessed' . DS;

        Mage::log( $path );
        if ( ! is_dir( $path ) ) {
            mkdir( $path , 0777 , true );
        }

        $filename = $data[ 'name' ];
        $fullName = $path . $filename;

        while ( file_exists( $fullName . '.csv' ) ) {
            $pieces = array ();

            $res = preg_match( '/^(.+)_(\d+)$/' , $fullName , $pieces );

            if ( ! $res ) {
                $fullName .= '_1';
            } else {
                $fullName .= '_' . strval( intval( $pieces[ 2 ] ) + 1 );
            }
        }

        $fullName .= '.csv';

        rename($file,$fullName);

        $data[ 'process' ] = 1;
        $data[ 'log' ] = $log ? serialize( $log ) : NULL;

        $data[ 'path' ] = $fullName;

        if($fileId){
            $fileModel = Mage::getModel( 'megatrix_pi/file' )->load( $fileId );
            $fileModel->setName( $data['name'] );
            $fileModel->setPath( $data['path'] );
            $fileModel->setProcess( $data['process'] );
            $fileModel->setLog( $data['log'] );
        }else{
            $fileModel =  Mage::getModel( 'megatrix_pi/file' );
            $fileModel->setData( $data );
        }

        try {
            $fileModel->save();
        } catch ( Exception $e ) {
            Mage::log( $e->getMessage() );
        }
        return;
    }

    /**
     * @param $product Mage_Catalog_Model_Product
     * @param $attrData array
     */

    protected function _updateAttr ( $product , $attrData ) {
        MX::getLogger()->dump($product->getData());
        MX::getLogger()->dump($attrData);

        $this->store_code = Mage::app()->getDefaultStoreView()->getCode();
        $productIds = array ( $product->getId() );
        Mage::getSingleton( 'catalog/product_action' )
            ->updateAttributes( $productIds , $attrData , $this->_getStore()
                                                               ->getId() );

        return;
    }

    /**
     * @param $product Mage_Catalog_Model_Product
     * @param $store string
     */

    protected function _updateWbs ( $product , $store ) {

        $this->store_code = $store;
        $websiteIds = $product->getWebsiteIds();
        if ( $wbs_id = $this->_getWebsiteId( $store ) ) {
            if ( ! in_array( $wbs_id , $websiteIds ) ) {
                $productIds = array ( $product->getId() );
                array_push( $websiteIds , $wbs_id );
                Mage::getSingleton( 'catalog/product_action' )
                    ->updateWebsites( $productIds , $websiteIds , 'add' );
            }
        }


        return;
    }

    /**
     * @param $sku string
     * @return bool|Mage_Catalog_Model_Product
     */

    protected function _existProduct ( $sku ) {

        //@uh verificamos que el producto exista y regresamos el objeto
        //@hakum We can load directly by SKU and if the result has an ID return the product object
        $product = Mage::getModel( 'catalog/product' )
                       ->loadByAttribute( 'sku' , $sku );
        if ( $product instanceof Mage_Catalog_Model_Product && $product->getId() ) {
            return $product;
        } else {
            return FALSE;
        }
    }

    /**
     * @param $store string
     * @return bool|int|null|string
     */
    protected function _getWebsiteId ( $store ) {

        $store_obj = Mage::getModel( 'core/store' )
                         ->load( $store );
        if ( $store_obj->getId() ) {
            return $store_obj->getWebsiteId();
        }
        return FALSE;
    }

    /**
     * @return bool|Mage_Core_Model_Store
     */
    protected function _getStore () {

        $store_obj = Mage::getModel( 'core/store' )
                         ->load( $this->store_code );

        return $store_obj->getId() ? $store_obj : FALSE;
    }

}
