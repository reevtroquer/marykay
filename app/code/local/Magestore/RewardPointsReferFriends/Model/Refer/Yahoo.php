<?php

/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_RewardPointsReferfriend
 * @module     RewardPointsReferfriend
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

/**
 * RewardpointsReferfriends Refer Yahoo
 * 
 * @category    Magestore
 * @package     Magestore_RewardPoints
 * @author      Magestore Developer
 */
class Magestore_Rewardpointsreferfriends_Model_Refer_Yahoo
{
    /**
     * Magestore_Rewardpointsreferfriends_Model_Refer_Yahoo constructor.
     */
    public function __construct(){
		try {
			require Mage::getBaseDir('lib').DS.'Yahoo'.DS.'Yahoo.inc';
		} catch (Exception $e) {
		}
		error_reporting(E_ALL | E_NOTICE);
		ini_set('display_errors', true);
		YahooLogger::setDebug(true);
		YahooLogger::setDebugDestination('LOG');
		
		ini_set('session.save_handler', 'files');
		session_save_path('/tmp/');
		session_start();
		
		if(Mage::app()->getRequest()->getParam('logout')){
			YahooSession::clearSession();
		}
	}

    /**
     * @return mixed
     */
    public function _getHelper(){
		return Mage::helper('rewardpointsreferfriends');
	}

    /**
     * @return mixed
     */
    protected function _getAppId(){
		return $this->_getHelper()->getReferConfig('yahoo_app_id');
	}

    /**
     * @return mixed
     */
    protected function _getConsumerKey(){
		return $this->_getHelper()->getReferConfig('yahoo_consumer_key');
	}

    /**
     * @return mixed
     */
    protected function _getConsumerSecret(){
		return $this->_getHelper()->getReferConfig('yahoo_consumer_secret');
	}

    /**
     * @return mixed
     */
    public function hasSession(){
		return YahooSession::hasSession($this->_getConsumerKey(), $this->_getConsumerSecret(), $this->_getAppId());
	}

    /**
     * @return mixed
     */
    public function getAuthUrl(){
		return YahooSession::createAuthorizationUrl($this->_getConsumerKey(), $this->_getConsumerSecret());
	}

    /**
     * @return mixed
     */
    public function getSession(){
		return YahooSession::requireSession($this->_getConsumerKey(), $this->_getConsumerSecret(), $this->_getAppId());
	}
}