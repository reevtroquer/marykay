<?php

class Appetite_Linio_Model_Miscellany_ConfigurationWatcher
{

    const CACHE_ADMINHTML_CONFIGURATIONWATCHER_PREFIX = 'appetite-ah-cw-{@1}';
    // CONFIGURATION WATCHER KEYS
    const CW_BEFORE = 'before';
    const CW_AFTER = 'after';
    const CW_PATH = 'path';
    const CW_WEBSITE = 'website';
    const CW_STORE = 'store';


    private static $separators = array( '-:-', '---', '-|-' );
    /** @var string */
    private $path = FALSE;

    /** @var Mage_Core_Model_Website */
    private $website = NULL;
    /** @var Mage_Core_Model_Store */
    private $store = NULL;

    /**
     * Appetite_Linio_Model_Miscellany_ConfigurationWatcher constructor.
     *
     * @param                              $path
     * @param Mage_Core_Model_Website|NULL $website
     * @param Mage_Core_Model_Store|NULL   $store
     */
    public function __construct ( $path,
                                  Mage_Core_Model_Website $website = NULL,
                                  Mage_Core_Model_Store $store = NULL ) {

        $this->path = $path;

        $this->website = $website;
        $this->store = $store;

        if ( is_null( $this->store ) && !is_null( $this->website ) ) {
            $this->store = $this->website->getDefaultStore();
        }

        $cache = Mage::helper( 'appetite_linio/cache' );
        $key = $this->key();

        $memento = $cache->get( $key );
        $stamp = $this->stamp();

        if ( !is_null( $memento ) && $memento != $stamp ) {
            $parameters = array(
                self::CW_BEFORE => $memento,
                self::CW_AFTER => $stamp,
                self::CW_PATH => $this->path,
                self::CW_WEBSITE => $this->website,
                self::CW_STORE => $this->store,
            );

            //Mage::dispatchEvent( 'appetite_configdata_change', $parameters );

            unset ( $parameters[ self::CW_PATH ] );

            $event_name = 'appetite_configdata_change_' . str_replace( '/', '_', $this->path );
            Mage::dispatchEvent( $event_name, $parameters );
        }

        $cache->set( $stamp, $key );
    }

    private function key () {

        $parts = array( $this->path,
                        !is_null( $this->website ) ? $this->website->getCode() : '',
                        !is_null( $this->store ) ? $this->store->getCode() : '' );
        $subject = join( ':', $parts );

        $prefix = str_replace( '/', '-', $subject );
        $key = Mage::helper( 'appetite_linio/cache' )
                   ->cache_key( self::CACHE_ADMINHTML_CONFIGURATIONWATCHER_PREFIX, $prefix );

        return $key;
    }

    private function stamp () {

        return $this->encode( Mage::getStoreConfig( $this->path, $this->store ) );
    }

    private function encode ( $node, $level = 0 ) {

        $buffer = array();
        if ( is_array( $node ) ) {
            foreach ( $node as $key => $item ) {
                $deeper = $level + 1;
                $buffer[] = join( self::$separators[ $deeper ],
                                  array( $key, $this->encode( $item, $deeper ) ) );
            }
        } elseif ( is_bool( $node ) ) {
            $buffer[] = $node ? 'TRUE' : 'FALSE';
        } else {
            $buffer[] = $node;
        }

        return join( self::$separators[ $level ], $buffer );
    }

}
