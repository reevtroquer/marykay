<?php

class Appetite_Linio_Model_Observer
{

    const CONFIG_CONFIGDATA_LINIO_USERS = 'appetite/linio/users';
    const CONFIG_CONFIGDATA_LINIO_MASTER_USER = 'appetite/linio/user_id';
    const CONFIG_CONFIGDATA_LINIO_DEBUG = 'appetite/configdata/debug';
    const CONFIG_CONFIGDATA_WATCHES = 'appetite/configdata/watches';
    const CACHE_CONFIGDATA_WATCHES = 'appetite-cd-w';
    const SESSION_LINIO_USER_ID = 'appetite-linio-uid';

    /** @var Varien_Data_Collection */
    private static $watches = NULL;
    /** @var Mage_Catalog_Model_Product */
    private $product = NULL;
    /** @var Mage_Sales_Model_Quote_Item */
    private $getQuoteItem = NULL;

    public function react ( Varien_Event_Observer $observer ) {

        $this->memento();
    }

    public function changed ( Varien_Event_Observer $observer ) {

        $post = Mage::app()
                    ->getRequest()
                    ->getPost();
    }

    // SOUL OF THE PATTERN
    private function memento () {

        list( $website , $store ) = $this->extract();
        $watches = $this->getWatches();

        foreach ( $watches->getData() as $watch_name => $metadata ) {
            if ( ! isset( $metadata[ 'path' ] ) ) {
                Mage::log( "cannot create configuration watch ($watch_name) --- path is needed" );
                continue;
            }
            $path = $metadata[ 'path' ];
            $watcher = ( isset( $metadata[ 'store' ] ) && $metadata[ 'store' ] )
                ? new Appetite_Linio_Model_Miscellany_ConfigurationWatcher( $path , $website , $store )
                : ( isset( $metadata[ 'website' ] ) && $metadata[ 'website' ] )
                    ? new Appetite_Linio_Model_Miscellany_ConfigurationWatcher( $path , $website )
                    : new Appetite_Linio_Model_Miscellany_ConfigurationWatcher( $path );
        }
    }

    /**
     * @return array
     */
    private function extract () {

        $request = Mage::app()
                       ->getRequest();
        $website_code = $request->getParam( 'website' );
        $store_code = $request->getParam( 'store' );

        Mage::log( "website_code := $website_code" );
        Mage::log( "store_code := $store_code" );

        $website = ! is_null( $website_code ) ? Mage::getModel( 'core/website' )
                                                    ->load( $website_code ) : NULL;
        $store = ! is_null( $store_code ) ? Mage::getModel( 'core/store' )
                                                ->load( $store_code ) : NULL;

        return array ( $website , $store );
    }

    /**
     * @return Varien_Object
     */
    private function getWatches () {

        if ( is_null( self::$watches ) ) {
            $cache = Mage::helper( 'appetite_linio/cache' );
            self::$watches = $cache->get( self::CACHE_CONFIGDATA_WATCHES );

            if ( is_null( self::$watches ) ) {
                $watches_configuration = Mage::getConfig()
                                             ->getNode( self::CONFIG_CONFIGDATA_WATCHES )
                                             ->asArray();
                self::$watches = ( new Varien_Object() )->addData( $watches_configuration );
                $cache->set( self::$watches , self::CACHE_CONFIGDATA_WATCHES );
            }
        }

        return self::$watches;
    }

    /**
     * @param Varien_Event_Observer $event_observer
     */
    public function handle ( Varien_Event_Observer $event_observer ) {

        //TODO: MAKE THE CALL FOR CREATE USER

    }

    /**
     * @param Varien_Event_Observer $event_observer
     */
    public function userChanged ( Varien_Event_Observer $event_observer ) {

        $helper = Mage::helper( 'appetite_linio' );

        $before = unserialize( $event_observer->getData( 'before' ) );
        $after = unserialize( $event_observer->getData( 'after' ) );


        if ( count( $before ) > count( $after ) ) {
            //TODO: Make all the logic to delete users from linio
            return;
        }

        if ( count( $before ) <= count( $after ) ) {
            // actually there is a new user, we need to create this new user in Linio.
            $newest_users = array_diff_key( $after , $before );
            if ( ! empty( $newest_users ) ) {
                foreach ( $newest_users as $k => $user ) {
                    Mage::log( $user , null , 'linio.log' );
                    $data = array (
                        'email' => $user[ 'email' ] ,
                        'first_name' => $user[ 'firstname' ] ,
                        'last_name' => $user[ 'lastname' ] ,
                    );
                    $result = $helper->createUser( $data );

                    if ( $result && $result->extractCode( $result->asString() ) == 200 ) {
                        $response = Mage::helper( 'core' )
                                        ->jsonDecode( gzdecode( $result->extractBody( $result->asString
                                        () ) ) );
                        if ( $response[ 'success' ] ) {
                            $id_linio = $response[ 'data' ][ 'id' ];
                            Mage::getSingleton( 'core/session' )
                                ->addSuccess( 'El usuario ' . $response[ 'data' ][ 'email' ] . ' fue creado en Linio.
                                 ID: ' . $id_linio );
                            $user[ 'linio_id' ] = $id_linio;
                            $after[ $k ] = $user;
                            $serialized = serialize( $after );
                            Mage::getConfig()
                                ->saveConfig( 'appetite/linio/users' , $serialized );

                        } else {
                            Mage::getSingleton( 'core/session' )
                                ->addError( 'El Usuario no fue creado en Linio. ' . $response[ 'messages' ][ 1 ][ 'message' ] );
                        }

                    } else {

                    }
                }
            } else {
                // There is no new user, something changed in of them
                $default = FALSE;
                foreach ( $before as $k => $user ) {
                    $newst = $after[ $k ];
                    try {
                        if ( $default && $newst[ 'is_defaul' ] === '1' ) {
                            throw new Exception( 'Solo se admite un usuario principal.' );
                            return;
                        }
                        if ( $newst[ 'is_defaul' ] === '1' && ! $default ) {
                            $default = TRUE;
                        }
                        if ( $newst[ 'is_defaul' ] === '1' && $newst[ 'enabled' ] !== '1' ) {
                            Mage::getSingleton( 'core/session' )
                                ->addNotice( 'El usuario principal debe de estar activo.' );
                        }

                        $changes = array_diff( $user , $newst );
                        //The changes in newst config are going to be listed in $changes
                        Mage::log( $changes );
                    } catch ( Exception $e ) {
                        Mage::getSingleton( 'core/session' )
                            ->addError( $e->getMessage() );
                    }

                }
            }
        }

    }

    public function beforeAddToCart ( Varien_Event_Observer $event_observer ) {

        /**
         * @var Mage_Sales_Model_Quote_Item $getQuoteItem
         */
        $this->getQuoteItem = $getQuoteItem = $event_observer->getQuoteItem();
        if ( ! $getQuoteItem->getId() ) {
            return;
        }
        $this->product = $event_observer->getProduct();
        $quote = $getQuoteItem->getQuote();
        $quote->setTotalsCollectedFlag( false )
              ->collectTotals();

        $master_linio_id = $this->_getConfig( self::CONFIG_CONFIGDATA_LINIO_MASTER_USER );
        $price = $quote->getBaseGrandTotal();
        $serialized_array = $this->_getConfig( self::CONFIG_CONFIGDATA_LINIO_USERS , $getQuoteItem->getStoreId() );
        $linio_users = ( $serialized_array ? unserialize( $serialized_array ) : array () );

        if ( $master_linio_id ) {
            $linio_users[] = $master_linio_id;
        }

        if ( empty( $linio_users ) ) {
            $this->back( 'Por el momento no podemos atender tu solicitud.' );
            return;
        }

        $users_sorted = array ();
        $default_flag = FALSE;

        foreach ( $linio_users as $k => $user ) {
            if ( $user[ 'enabled' ] !== '1' ) {
                continue;
            }
            if ( $user[ 'is_defaul' ] === '1' ) {
                $default_flag = TRUE;
                array_unshift( $users_sorted , $user );
                continue;
            }
            $users_sorted[] = $user;
        }

        if ( ! $default_flag ) {
            $this->back( 'Por el momento no podemos atender tu solicitud. Codigo 200.' );
            return;
        }

        $arrayKeys = array_keys( $users_sorted );
        $lastArrayKey = array_pop( $arrayKeys );

        foreach ( $users_sorted as $k => $user ) {
            $budget = (integer) $user[ 'budget' ];
            if ( $budget == 0 ) {
                if ( $k == $lastArrayKey ) {
                    $this->back( 'Por el momento no podemos atender tu solicitud. Codigo 300.' );
                    return;
                }
                continue;
            }

            $diff = $budget - $price;

            if ( $diff > 0 ) {
                if ( ! $user[ 'linio_id' ] ) {
                    continue;
                }
                $session = Mage::getSingleton( 'checkout/session' );
                $session->setData( self::SESSION_LINIO_USER_ID , $user[ 'linio_id' ] );
                return;
            }

            if ( $k == $lastArrayKey ) {
                $this->back( 'Por el momento no podemos atender tu solicitud. Codigo 400.' );
                return;
            }
        }

    }

    private function back ( $message ) {

        Mage::getSingleton( 'checkout/session' )
            ->getMessages( TRUE );

        $session = Mage::getSingleton( 'core/session' );
        $session->getMessages( true );

        $session->addError( $message );
        $this->getQuoteItem->getQuote()
                           ->removeItem( $this->getQuoteItem->getData( 'item_id' ) );
        $this->getQuoteItem->getQuote()
                           ->collectTotals()
                           ->save();
        //Mage::getSingleton('checkout/session')->clear();

        return;
    }

    private function _getConfig ( $path = null , $store = null ) {

        if ( $path ) {
            $store = $store
                ? $store
                : Mage::app()
                      ->getDefaultStoreView();

            return Mage::getStoreConfig( $path , $store );
        }

        return null;

    }

    public function salesOrderPlaceAfter ( Varien_Event_Observer $event_observer ) {

        $helper = Mage::helper( 'appetite_linio' );

        $orderId = $event_observer->getOrder()
                                  ->getData( 'entity_id' );

        $order = $this->order = $event_observer->getOrder();
        $order_model = $this->order_model = Mage::getModel( 'sales/order' )
                                                ->load( $orderId );
        Mage::log( 'INICIA ENVIO DE ORDEN HACIA WS DE LINIO PARA ORDEN ' . $order_model->getIncrementId() ,
                   null , 'linio.log' );

        $invoice = $this->invoice = $event_observer->getInvoice();

        $billing_address = $this->billing_address = $order->getBillingAddress();

        if ( $order->getId() ) {
            $result = $helper->createOrder( $this->_createOrderArrayLinio( $order ) );
            Mage::log( $result , null , 'linio.log' );
            if ( $result && $result->extractCode( $result->asString() ) == 200 ) {
                if($result->getHeader('Content-encoding') == 'gzip'){
                    $response = Mage::helper('core')->jsonDecode(gzdecode($result->extractBody($result->asString())));
                }else {
                    $response = Mage::helper('core')
                        ->jsonDecode($result->extractBody($result->asString()));
                }
                try {
                    if ( $response ) {

                        $order->setLinioId( $response[ 'id' ] );
                        $order->setLinioOrderNumber( $response[ 'orderNumber' ] );
                        $order->setLinioCustomerOrderNumber( $response[ 'orderNumber' ] );

                        $order->save();

                        $session = Mage::getSingleton( 'checkout/session' );
                        $session->unsetData( self::SESSION_LINIO_USER_ID );
                    } else {
                        throw new Exception( 'No se pudo crear la orden.' );
                    }
                } catch ( Exception $e ) {
                    Mage::throwException( $response );
                }
            } elseif ( $result && $result->extractCode( $result->asString() ) == 400 ) {
                $response = Mage::helper( 'core' )
                                ->jsonDecode( $result->extractBody( $result->asString() ) );

                //switch ($response['code']){
                //case 'ORDER_PROCESS_FAILURE' :
                $message = 'El SKU : ' . $response[ 'errors' ][ 1 ][ 'field' ] . ' No esta disponible para su venta.';
                Mage::throwException( $message );
                //}

            } else {
                Mage::throwException( 'Por el momento no se puede completar tu pedido.' );
            }
        }
    }

    private function _getOrderArray () {

        return array (
            'customer' => array () ,
            'order' => array () ,
            'address' => array () ,
            'items' => array () ,
        );
    }

    private function _createOrderArrayLinio ( Mage_Sales_Model_Order $order ) {

        $order_array = $this->_getOrderArray();
        $order_array[ 'customer' ][ 'firstName' ] = $order->getCustomerFirstname();
        $order_array[ 'customer' ][ 'lastName' ] = $order->getCustomerLastname();

        $order_array[ 'order' ][ 'customerOrderNumber' ] = $this->_getConfig( self::CONFIG_CONFIGDATA_LINIO_DEBUG ) ?
            'debug-' . $order->getIncrementId() : $order->getIncrementId();

        $order_array[ 'order' ][ 'couponCode' ] = $order->getCouponCode();

        $order_array[ 'address' ][ 'address1' ] = $order->getShippingAddress()
                                                        ->getStreet( 1 );
        $order_array[ 'address' ][ 'streetNumber' ] = $order->getShippingAddress()
                                                            ->getStreet( 2 ) ? $order->getShippingAddress()
                                                                                     ->getStreet( 2 ) : "01";
        $order_array[ 'address' ][ 'municipality' ] = $order->getShippingAddress()
                                                            ->getRegion();
        $order_array[ 'address' ][ 'mobilePhone' ] = $order->getShippingAddress()
                                                           ->getTelephone();
        $order_array[ 'address' ][ 'region' ] = $order->getShippingAddress()
                                                      ->getCity();
        $order_array[ 'address' ][ 'postcode' ] = $order->getShippingAddress()
                                                        ->getPostcode();

        $order_array[ 'address' ][ 'additionalInformation' ] = null;
//        $order_array[ 'address' ][ 'company' ] = '';
//        $order_array[ 'address' ][ 'betweenStreet1' ] = '';
//        $order_array[ 'address' ][ 'betweenStreet2' ] = '';

//        $order_array[ 'fixedShipping' ][ 'amount' ] = '';
//        $order_array[ 'fixedShipping' ][ 'cost' ] = '';
//        $order_array[ 'fixedShipping' ][ 'shipmentCarrier' ] = '';

        /**
         * @var Mage_Sales_Model_Order_Item $item
         */
        foreach ( $order->getAllVisibleItems() as $k => $item ) {
            $order_array[ 'items' ][ $k ][ 'sku' ] = $item->getSku();
            $order_array[ 'items' ][ $k ][ 'name' ] = $item->getName();
            $order_array[ 'items' ][ $k ][ 'quantity' ] = $item->getQtyOrdered();
            $order_array[ 'items' ][ $k ][ 'price' ] = $item->getPrice();
        }

        return $order_array;
    }

    public function _LinioId ( $collection , $column ) {

        if ( ! $value = $column->getFilter()
                               ->getValue()
        ) {
            return;
        }

        $collection->getSelect()
                   ->where( "sales_flat_order.linio_id like ?" , "%$value%" );

        return;
    }

    public function _LinioOrderNumber ( $collection , $column ) {

        if ( ! $value = $column->getFilter()
                               ->getValue()
        ) {
            return;
        }

        $collection->getSelect()
                   ->where( "sales_flat_order.linio_order_number like ?" , "%$value%" );

        return;
    }

    public function _LinioCustomerOrder ( $collection , $column ) {

        if ( ! $value = $column->getFilter()
                               ->getValue()
        ) {
            return;
        }

        $collection->getSelect()
                   ->where( "sales_flat_order.linio_customer_order_number like ?" , "%$value%" );

        return;
    }

    public function _filter_by_store ( $collection , $column ) {

        if ( ! $value = $column->getFilter()
                               ->getValue()
        ) {
            return;
        }

        $collection->getSelect()
                   ->where( "main_table.store_id = ?" , "$value" );

        return;
    }

    public function _filter_by_created ( $collection , $column ) {

        if ( ! $value = $column->getFilter()
                               ->getValue()
        ) {
            return;
        }
        //$date = new DateTime($value);
        //$value = $date->format('Y-m-d H:i:s');
        $from = $value[ 'from' ]->get( 'YYYY-MM-dd HH:mm:ss' );
        $to = $value[ 'to' ]->get( 'YYYY-MM-dd HH:mm:ss' );
        //var_dump($value);die();

        $collection->getSelect()
                   ->where( "main_table.created_at >= ?" , "$from" )
                   ->where( "main_table.created_at <= ?" , "$to" );

        return;
    }

    public function salesOrderGridCollectionLoadBefore ( $observer ) {

        $collection = $observer->getOrderGridCollection();
        $select = $collection->getSelect();
        $select->joinLeft( 'sales_flat_order' , 'sales_flat_order.entity_id = main_table.entity_id' ,
                           array (
                               'linio_id' => new Zend_Db_Expr( 'sales_flat_order.linio_id' ) ,
                               'linio_order_number' => new Zend_Db_Expr( 'sales_flat_order.linio_order_number' ) ,
                               'linio_customer_order_number' => new Zend_Db_Expr( 'sales_flat_order.linio_customer_order_number' ) ,
                           )
        );
        $select->group( 'main_table.entity_id' );
    }
}
