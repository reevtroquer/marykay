<?php
/**
 * Created by PhpStorm.
 * User: Hakum
 * Date: 10/10/17
 * Time: 4:05 PM
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()
          ->addColumn($installer->getTable('sales/order'),
                      'linio_id',
                      array(
                          'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
                          'nullable' => true,
                          'default' => null,
                          'comment' => 'linio_id'
                      )
          );

$installer->getConnection()
          ->addColumn($installer->getTable('sales/order'),
                      'linio_order_number',
                      array(
                          'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
                          'nullable' => true,
                          'default' => null,
                          'comment' => 'linio_order_number'
                      )
          );

$installer->getConnection()
          ->addColumn($installer->getTable('sales/order'),
                      'linio_customer_order_number',
                      array(
                          'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
                          'nullable' => true,
                          'default' => null,
                          'comment' => 'linio_customer_order_number'
                      )
          );

$installer->getConnection()
          ->addColumn($installer->getTable('sales/quote'),
                      'linio_id',
                      array(
                          'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
                          'nullable' => true,
                          'default' => null,
                          'comment' => 'linio_id'
                      )
          );

$installer->getConnection()
          ->addColumn($installer->getTable('sales/quote'),
                      'linio_order_number',
                      array(
                          'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
                          'nullable' => true,
                          'default' => null,
                          'comment' => 'linio_order_number'
                      )
          );

$installer->getConnection()
          ->addColumn($installer->getTable('sales/quote'),
                      'linio_customer_order_number',
                      array(
                          'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
                          'nullable' => true,
                          'default' => null,
                          'comment' => 'linio_customer_order_number'
                      )
          );


$installer->endSetup();