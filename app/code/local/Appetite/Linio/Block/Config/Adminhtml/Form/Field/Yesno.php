<?php

/**
 * Created by PhpStorm.
 * User: Abraham
 * Date: 4/12/2016
 * Time: 1:36 AM
 */
class Appetite_Linio_Block_Config_Adminhtml_Form_Field_Yesno extends Mage_Core_Block_Html_Select
{


    public function setInputName ( $value ) {

        return $this->setName( $value );
    }

    public function _toHtml()
    {
        if (!$this->_beforeToHtml()) {
            return '';
        }

        $current = Mage::app()
                       ->getRequest()
                       ->getParam( 'website' );

        $this->addOption( 0 , 'No' );
        $this->addOption( 1 , 'Yes' );
        $this->setClass( 'appetite_code_select' );

        $html = '<select name="' . $this->getName() . '" id="' . $this->getId() . '" class="'
            . $this->getClass() . '" title="' . $this->getTitle() . '" ' . $this->getExtraParams() . '>';
        $values = $this->getValue();

        if (!is_array($values)){
            if (!is_null($values)) {
                $values = array($values);
            } else {
                $values = array();
            }
        }

        $isArrayOption = true;
        foreach ($this->getOptions() as $key => $option) {
            if ($isArrayOption && is_array($option)) {
                $value  = $option['value'];
                $label  = (string)$option['label'];
                $params = (!empty($option['params'])) ? $option['params'] : array();
            } else {
                $value = (string)$key;
                $label = (string)$option;
                $isArrayOption = false;
                $params = array();
            }

            if (is_array($value)) {
                $html .= '<optgroup label="' . $label . '">';
                foreach ($value as $keyGroup => $optionGroup) {
                    if (!is_array($optionGroup)) {
                        $optionGroup = array(
                            'value' => $keyGroup,
                            'label' => $optionGroup
                        );
                    }
                    $html .= $this->_optionToHtml(
                        $optionGroup,
                        in_array($optionGroup['value'], $values)
                    );
                }
                $html .= '</optgroup>';
            } else {
                $html .= $this->_optionToHtml(
                    array(
                        'value' => $value,
                        'label' => $label,
                        'params' => $params
                    ),
                    in_array($value, $values)
                );
            }
        }
        $html .= '</select>';
        return $html;
    }
}
