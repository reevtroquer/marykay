<?php

/**
 * Created by PhpStorm.
 * User: Abraham
 * Date: 4/12/2016
 * Time: 1:36 AM
 */
class Appetite_Linio_Block_Config_Adminhtml_Form_Field_Attributes extends Mage_Core_Block_Html_Select
{
    public function _toHtml () {
        $options = Mage::getResourceModel( 'catalog/product_attribute_collection' )
                       ->getItems();

        foreach ( $options as $option ) {
            $this->addOption( $option->getAttributecode(), $option->getAttributecode() );
        }
        $this->setClass( 'magento_code_select' );

        return parent::_toHtml();
    }

    public function setInputName ( $value ) {
        return $this->setName( $value );
    }
}
