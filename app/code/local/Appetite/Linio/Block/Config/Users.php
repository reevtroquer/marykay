<?php

/**
 * Created by PhpStorm.
 * User: Abraham
 * Date: 3/31/2016
 * Time: 2:18 PM
 */
class Appetite_Linio_Block_Config_Users extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{

    protected $_itemRenderer;
    protected $_itemRendererMC;

    protected $_itemRendererAdmin;
    protected $_itemRendererStatus;
    protected $_itemRendererTracking;

    public function _prepareToRender () {

        $this->addColumn( 'email' , array (
            'label' => $this->__( 'Email' ) ,
            'style' => 'width:100px' ,
        ) );
        $this->addColumn( 'firstname' , array (
            'label' => $this->__( 'Nombre' ) ,
            'style' => 'width:100px' ,
        ) );
        $this->addColumn( 'lastname' , array (
            'label' => $this->__( 'Apellido' ) ,
            'style' => 'width:100px' ,
        ) );
        $this->addColumn( 'budget' , array (
            'label' => $this->__( 'Budget' ) ,
            'style' => 'width:60px' ,
        ) );
        $this->addColumn( 'enabled' , array (
            'label' => $this->__( 'Activo' ) ,
            'style' => 'width:30px' ,
            'renderer' => $this->_getRendererEnabled() ,
        ) );

        $this->addColumn( 'is_defaul' , array (
            'label' => $this->__( 'Es Principal' ) ,
            'style' => 'width:30px' ,
            'renderer' => $this->_getRendererIsDefault() ,
        ) );

        $this->addColumn( 'is_admin' , array (
            'label' => $this->__( 'Es Admin' ) ,
            'style' => 'width:30px' ,
            'renderer' => $this->_getRendererIsAdmin() ,
        ) );

        $this->addColumn( 'can_see_status' , array (
            'label' => $this->__( 'Ver Status' ) ,
            'style' => 'width:30px' ,
            'renderer' => $this->_getRendererCanSeeStatus() ,
        ) );

        $this->addColumn( 'can_see_tracking' , array (
            'label' => $this->__( 'Ver Track' ) ,
            'style' => 'width:30px' ,
            'renderer' => $this->_getRendererCanSeeTracking() ,
        ) );

        $this->addColumn( 'linio_id' , array (
            'label' => $this->__( 'Linio ID' ) ,
            'style' => 'width:100px' ,
        ) );

        $this->_addAfter = FALSE;
        $this->_addButtonLabel = $this->__( 'Añadir Usuario' );
    }

    protected function _getRendererEnabled () {

        if ( ! $this->_itemRenderer ) {
            $this->_itemRenderer = $this->getLayout()
                                        ->createBlock(
                                            'appetite_linio/config_adminhtml_form_field_yesno' , '' ,
                                            array ( 'is_render_to_js_template' => TRUE )
                                        );
        }

        return $this->_itemRenderer;
    }

    protected function _getRendererIsDefault () {

        if ( ! $this->_itemRendererMC ) {
            $this->_itemRendererMC = $this->getLayout()
                                          ->createBlock(
                                              'appetite_linio/config_adminhtml_form_field_yesno' , '' ,
                                              array ( 'is_render_to_js_template' => TRUE )
                                          );
        }

        return $this->_itemRendererMC;
    }

    protected function _getRendererIsAdmin () {

        if ( ! $this->_itemRendererAdmin ) {
            $this->_itemRendererAdmin = $this->getLayout()
                                          ->createBlock(
                                              'appetite_linio/config_adminhtml_form_field_yesno' , '' ,
                                              array ( 'is_render_to_js_template' => TRUE )
                                          );
        }

        return $this->_itemRendererAdmin;
    }

    protected function _getRendererCanSeeStatus () {

        if ( ! $this->_itemRendererStatus ) {
            $this->_itemRendererStatus = $this->getLayout()
                                          ->createBlock(
                                              'appetite_linio/config_adminhtml_form_field_yesno' , '' ,
                                              array ( 'is_render_to_js_template' => TRUE )
                                          );
        }

        return $this->_itemRendererStatus;
    }

    protected function _getRendererCanSeeTracking () {

        if ( ! $this->_itemRendererTracking ) {
            $this->_itemRendererTracking = $this->getLayout()
                                          ->createBlock(
                                              'appetite_linio/config_adminhtml_form_field_yesno' , '' ,
                                              array ( 'is_render_to_js_template' => TRUE )
                                          );
        }

        return $this->_itemRendererTracking;
    }

    protected function _prepareArrayRow ( Varien_Object $row ) {

        $row->setData(
            'option_extra_attr_' . $this->_getRendererEnabled()
                                        ->calcOptionHash( $row->getData( 'enabled' ) ) ,
            'selected="selected"'
        );

        $row->setData(
            'option_extra_attr_' . $this->_getRendererIsDefault()
                                        ->calcOptionHash( $row->getData( 'is_defaul' ) ) ,
            'selected="selected"'
        );

        $row->setData(
            'option_extra_attr_' . $this->_getRendererIsAdmin()
                                        ->calcOptionHash( $row->getData( 'is_admin' ) ) ,
            'selected="selected"'
        );

        $row->setData(
            'option_extra_attr_' . $this->_getRendererCanSeeStatus()
                                        ->calcOptionHash( $row->getData( 'can_see_status' ) ) ,
            'selected="selected"'
        );

        $row->setData(
            'option_extra_attr_' . $this->_getRendererCanSeeTracking()
                                        ->calcOptionHash( $row->getData( 'can_see_tracking' ) ) ,
            'selected="selected"'
        );
    }
}
