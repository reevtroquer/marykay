<?php
/**
 * Created by PhpStorm.
 * User: Hakum
 * Date: 10/11/17
 * Time: 3:22 PM
 */

class Appetite_Linio_Block_Adminhtml_Track extends Mage_Adminhtml_Block_Template
{

    public function getTracking () {

        $session = Mage::getSingleton( 'adminhtml/session' );
        $helper = Mage::helper( 'appetite_linio' );

        $result = $helper->getTrack( $session->getData( 'linio_order_number' ) );
        $response = FALSE;
        if ( $result && $result->extractCode( $result->asString() ) == 200 ) {
            $response = Mage::helper( 'core' )
                            ->jsonDecode( $result->extractBody( $result->asString() ) );
            try {
                if ( ! empty( $response[ 0 ] ) ) {
                    $response = $response[ 0 ];
                    if($response['trackingCode'] == 'NULL_TRACKING_CODE')
                        return FALSE;
                    $this->createShipment($response);
                    return $response;
                } else {
                    throw new Exception( 'No se pudo crear la orden.' );
                }
            } catch ( Exception $e ) {
                Mage::throwException( $e->getMessage() );
            }
        } else {
            Mage::throwException( 'Por el momento no se puede completar tu pedido.' );
        }

        return FALSE;
    }

    public function createShipment ($response) {

        if(! is_null( $response[ 'trackingCode' ] )){
            $session = Mage::getSingleton( 'adminhtml/session' );

            $order_collection = Mage::getModel( 'sales/order' )
                                    ->getCollection();
            $order = $order_collection->addFieldToFilter( 'linio_order_number' ,
                                                          array ( 'eq' => $session->getData( 'linio_order_number' )
                                                          ) )
                                      ->getFirstItem();

            if ( $order->canShip() ) {
                $itemQty = $order->getItemsCollection()
                                 ->count();
                $shipment = Mage::getModel( 'sales/service_order' , $order )
                                ->prepareShipment( $itemQty );
                $shipment = new Mage_Sales_Model_Order_Shipment_Api();
                $shipmentId = $shipment->create( $order->getData('increment_id') );

                $shipment =  Mage::getModel('sales/order_shipment')->loadByIncrementId($shipmentId);

                $track = Mage::getModel( 'sales/order_shipment_track' )
                             ->setShipment( $shipment )
                             ->setData( 'title' , $response[ 'carrier' ] )
                             ->setData( 'number' , $response[ 'trackingCode' ] )
                             ->setData( 'carrier_code' , 'custom' )
                             ->setData( 'order_id' , $order->getData('entity_id') )
                             ->save();
                $shipment->addComment($response[ 'carrier' ] . ' ' . $response[ 'trackingCode' ]);
                $shipment->sendEmail(TRUE);
                $shipment->save();

                $order->setIsInProcess(true);
                $order->addStatusHistoryComment( 'Guía ' . $response[ 'carrier' ] . ' Generada: ' . $response[ 'trackingCode' ] );
                $order->save();
            }
        }

    }
}