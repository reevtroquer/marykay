<?php
/**
 * Created by PhpStorm.
 * User: Hakum
 * Date: 22/06/17
 * Time: 3:54 PM
 */

class Appetite_Linio_Block_Adminhtml_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel($this->_getCollectionClass());
        //$collection->addFilterToMap('store_id', 'main_table.store_id');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $this->addColumn('real_order_id', array(
            'header'=> Mage::helper('sales')->__('Order #'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'increment_id',
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('new_store_id', array(
                'header'    => Mage::helper('sales')->__('Purchased From (Store)'),
                'index'     => 'main_table.store_id',
                'type'      => 'store',
                'store_view'=> true,
                'display_deleted' => true,
                'filter_condition_callback' => array('Megatrix_Costcenter_Model_Observer_Order' , '_filter_by_store')
            ));
        }

        $this->addColumn('new_created_at', array(
            'header' => Mage::helper('sales')->__('Purchased On'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '100px',
	    'filter_condition_callback' => array('Megatrix_Costcenter_Model_Observer_Order' , '_filter_by_created')
        ));

        $this->addColumn('billing_name', array(
            'header' => Mage::helper('sales')->__('Bill to Name'),
            'index' => 'billing_name',
        ));

        $this->addColumn('shipping_name', array(
            'header' => Mage::helper('sales')->__('Ship to Name'),
            'index' => 'shipping_name',
        ));

        $this->addColumn('base_grand_total', array(
            'header' => Mage::helper('sales')->__('G.T. (Base)'),
            'index' => 'base_grand_total',
            'type'  => 'currency',
            'currency' => 'base_currency_code',
        ));

        $this->addColumn('grand_total', array(
            'header' => Mage::helper('sales')->__('G.T. (Purchased)'),
            'index' => 'grand_total',
            'type'  => 'currency',
            'currency' => 'order_currency_code',
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('sales')->__('Status'),
            'index' => 'status',
            'type'  => 'options',
            'width' => '70px',
            'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
        ));

        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
            $this->addColumn('action',
                             array(
                                 'header'    => Mage::helper('sales')->__('Action'),
                                 'width'     => '50px',
                                 'type'      => 'action',
                                 'getter'     => 'getId',
                                 'actions'   => array(
                                     array(
                                         'caption' => Mage::helper('sales')->__('View'),
                                         'url'     => array('base'=>'*/sales_order/view'),
                                         'field'   => 'order_id',
                                         'data-column' => 'action',
                                     )
                                 ),
                                 'filter'    => false,
                                 'sortable'  => false,
                                 'index'     => 'stores',
                                 'is_system' => true,
                             ));
        }

        $this->addColumn('linio_id', array(
            'header' => Mage::helper('sales')->__('Linio id'),
            'index' => 'linio_id',
            'filter_condition_callback' => array('Appetite_Linio_Model_Observer' , '_LinioId')
        ));

        $this->addColumn('linio_order_number', array(
            'header' => Mage::helper('sales')->__('L. Order Number'),
            'index' => 'linio_order_number',
            'filter_condition_callback' => array('Appetite_Linio_Model_Observer' , '_LinioOrderNumber')
        ));

        $this->addColumn('linio_customer_order_number', array(
            'header' => Mage::helper('sales')->__('L. Customer O. N.'),
            'index' => 'linio_customer_order_number',
            'filter_condition_callback' => array('Appetite_Linio_Model_Observer' , '_LinioCustomerOrderNumber')
        ));


        $this->addRssList('rss/order/new', Mage::helper('sales')->__('New Order RSS'));

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel XML'));


        if (1 == 1 || Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/status')) {
            $this->addColumn('action_linio_status',
                             array(
                                 'header'    => Mage::helper('sales')->__('Action'),
                                 'width'     => '60px',
                                 'type'      => 'action',
                                 'getter'     => 'getLinioOrderNumber',
                                 'actions'   => array(
                                     array(
                                         'caption' => Mage::helper('sales')->__('L. Status'),
                                         'url'     => array('base'=>'*/sales_order/lstatus'),
                                         'field'   => 'linio_order_number',
                                         'onclick' => 'javascript:statusPopup(this)'
                                     )
                                 ),
                                 'filter'    => false,
                                 'sortable'  => false,
                                 'index'     => 'stores',
                                 'is_system' => true,
                             ));
        }

        if (1 == 1 || Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/track')) {
            $this->addColumn('action_linio_track',
                             array(
                                 'header'    => Mage::helper('sales')->__('Action'),
                                 'width'     => '60px',
                                 'type'      => 'action',
                                 'getter'     => 'getLinioOrderNumber',
                                 'actions'   => array(
                                     array(
                                         'caption' => Mage::helper('sales')->__('L. Track'),
                                         'url'     => array('base'=>'*/sales_order/ltrack'),
                                         'field'   => 'linio_order_number',
                                         'onclick' => 'javascript:trackPopup(this)'
                                     )
                                 ),
                                 'filter'    => false,
                                 'sortable'  => false,
                                 'index'     => 'stores',
                                 'is_system' => true,
                             ));
        }

        return $this;
    }
}
