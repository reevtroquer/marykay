<?php
/**
 * Created by PhpStorm.
 * User: Hakum
 * Date: 01/11/17
 * Time: 12:10 PM
 */

require_once 'Mage/Adminhtml/controllers/Sales/OrderController.php';

class Appetite_Linio_Adminhtml_Sales_OrderController extends Mage_Adminhtml_Sales_OrderController
{

    public function ltrackAction () {

        $params = $this->getRequest()
                       ->getParams();
        $linio_order_number = $params[ 'linio_order_number' ];

        Mage::getSingleton('adminhtml/session')->setData('linio_order_number' , $linio_order_number);

        $this->loadLayout()
             ->_title($this->__('Linio Tracking'));

        $this->renderLayout();
    }

    public function lstatusAction () {

        $params = $this->getRequest()
                       ->getParams();
        $linio_order_number = $params[ 'linio_order_number' ];

        Mage::getSingleton('adminhtml/session')->setData('linio_order_number' , $linio_order_number);

        $this->loadLayout()
             ->_title($this->__('Linio Status'));

        $this->renderLayout();
    }

}