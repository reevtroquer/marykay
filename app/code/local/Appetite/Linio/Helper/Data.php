<?php

/**
 * Created by PhpStorm.
 * User: Hakum
 * Date: 10/10/17
 * Time: 4:05 PM
 */
class Appetite_Linio_Helper_Data extends Mage_Core_Helper_Abstract
{

    private $uri = null;
    private $token = null;

    const MAGENTO_PATH_LINIO_DEBUG = 'appetite/linio/debug';
    const MAGENTO_PATH_LINIO_URI_PROD = 'appetite/linio/uri_prod';
    const MAGENTO_PATH_LINIO_URI_DEV = 'appetite/linio/uri_dev';
    const MAGENTO_PATH_LINIO_TOKEN = 'appetite/linio/token';
    const MAGENTO_PATH_LINIO_MASTER_USER = 'appetite/linio/user_id';

    const URI_LINIO_USER_CREATE = '/api/1.0/user/create.json';
    const URI_LINIO_ORDER_CREATE = '/order/create.json';

    const URI_LINIO_ORDER_STATUS = '/order/status.json';
    const URI_LINIO_ORDER_TRACK = '/order/tracking.json';

    const SESSION_LINIO_USER_ID = 'appetite-linio-uid';

    public function createUser ( $user ) {

        $service = new Varien_Http_Client( $this->_getUri() . self::URI_LINIO_USER_CREATE );
        $service->setMethod( Varien_Http_Client::POST );
        $service->setHeaders( $this->getHeaders() );
        $service->setRawData( Mage::helper( 'core' )
                                  ->jsonEncode( $user ) , 'application/json' );

        try {
            $response = $service->request();
            $last_request = $service->getLastRequest();
            $last_response = $service->getLastResponse();
            Mage::log( $last_request , null , 'linio.log' );
            Mage::log( $last_response , null , 'linio.log' );
            return $response;
        } catch ( Exception $e ) {
            Mage::throwException( $e->getMessage() );
        }

    }

    public function createOrder ( $order ) {

        $service = new Varien_Http_Client( $this->_getUri() . self::URI_LINIO_ORDER_CREATE );
        $service->setMethod( Varien_Http_Client::POST );
        $service->setHeaders( $this->getHeaders( TRUE ) );
        $service->setRawData( Mage::helper( 'core' )
                                  ->jsonEncode( $order ) , 'application/json' );

        try {
            $response = $service->request();
            $last_request = $service->getLastRequest();
            $last_response = $service->getLastResponse();
            Mage::log( $last_request , null , 'linio.log' );
            Mage::log( $last_response , null , 'linio.log' );
            return $response;
        } catch ( Exception $e ) {
            Mage::throwException( $e->getMessage() );
        }

    }

    public function getTrack ( $linio_order_number ) {

        $service = new Varien_Http_Client( $this->_getUri() . self::URI_LINIO_ORDER_TRACK );
        $service->setMethod( Varien_Http_Client::POST );
        $headers = $this->getHeaders( FALSE , TRUE );
        $service->setHeaders( $this->getHeaders( FALSE , TRUE ) );
        $service->setRawData( Mage::helper( 'core' )
                                  ->jsonEncode( array ('orderNumber' => $linio_order_number , 'customerOrderNumber'
                                  => null ) ) , 'application/json' );

        try {
            $response = $service->request();
            $last_request = $service->getLastRequest();
            $last_response = $service->getLastResponse();
            Mage::log( $headers , null , 'linio.log' );
            Mage::log( $last_request , null , 'linio.log' );
            Mage::log( $last_response , null , 'linio.log' );
            return $response;
        } catch ( Exception $e ) {
            Mage::throwException( $e->getMessage() );
        }

    }

    public function getStatus ( $linio_order_number ) {

        $service = new Varien_Http_Client( $this->_getUri() . self::URI_LINIO_ORDER_STATUS );
        $service->setMethod( Varien_Http_Client::POST );
        $headers = $this->getHeaders( FALSE , TRUE );
        $service->setHeaders( $this->getHeaders( FALSE , TRUE ) );
        $service->setRawData( Mage::helper( 'core' )
                                  ->jsonEncode( array ('orderNumber' => $linio_order_number , 'customerOrderNumber'
                                  => null ) ) , 'application/json' );

        try {
            $response = $service->request();
            $last_request = $service->getLastRequest();
            $last_response = $service->getLastResponse();
            Mage::log( $headers , null , 'linio.log' );
            Mage::log( $last_request , null , 'linio.log' );
            Mage::log( $last_response , null , 'linio.log' );
            return $response;
        } catch ( Exception $e ) {
            Mage::throwException( $e->getMessage() );
        }

    }

    private function _getUri () {

        if ( ! $this->uri ) {
            $this->uri = $this->_setUri();
        }

        return $this->uri;
    }

    private function _setUri () {

        if ( ! $this->uri && $this->getConfig( self::MAGENTO_PATH_LINIO_DEBUG ) ) {

            $this->uri = $this->getConfig( self::MAGENTO_PATH_LINIO_URI_DEV );
            return $this->uri;
        }

        $this->uri = $this->getConfig( self::MAGENTO_PATH_LINIO_URI_PROD );
        return $this->uri;
    }

    private function getConfig ( $path , $store = FALSE ) {

        $store = $store !== FALSE
            ? $store
            : Mage::app()
                  ->getWebsite()
                  ->getDefaultGroup()
                  ->getDefaultStoreId();

        return Mage::getStoreConfig( $path , $store );
    }

    private function _getToken () {

        if ( ! $this->token ) {
            $this->token = $this->_setToken();
        }

        return $this->token;
    }

    private function _setToken () {

        if ( ! $this->token && $this->token = $this->getConfig( self::MAGENTO_PATH_LINIO_TOKEN ) ) {

            return $this->token;
        }

        return $this->token;
    }

    private function getHeaders ( $order = FALSE , $is_adminhtml = FALSE ) {

        $session = Mage::getSingleton( 'checkout/session' );
        if ( $order ) {
            return array (
                'Content-Type' => 'application/json' ,
                'Linio-Token' => $this->_getToken() ,
                'Linio-Account' => $this->_getConfig(self::MAGENTO_PATH_LINIO_MASTER_USER)
            );
        }

        if($is_adminhtml){
            return array (
                'Content-Type' => 'application/json' ,
                'Linio-Token' => $this->_getToken() ,
                'Linio-Account' => $this->_getConfig(self::MAGENTO_PATH_LINIO_MASTER_USER)
            );
        }

        return array (
            'Content-Type' => 'application/json' ,
            'Linio-Token' => $this->_getToken()
        );
    }

    private function _getConfig ( $path = null , $store = null ) {

        if ( $path ) {
            $store = $store
                ? $store
                : Mage::app()
                      ->getDefaultStoreView();

            return Mage::getStoreConfig( $path , $store );
        }

        return null;

    }
}
