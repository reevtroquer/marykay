<?php

class Appetite_Linio_Helper_Cache extends Mage_Core_Helper_Abstract
{

    const TYPE_SIMPLE = 's';
    const TYPE_VARIENOBJECT = 'v';
    const TYPE_ARRAY = 'a';
    const TYPE_OBJECT = 'o';
    const TYPE_BOOLEAN = 'b';
    private $_cache = NULL;
    private $_tags = array( 'appetite' );
    private $_prefix = 'Appetite';

    /**
     * @param string $key
     * @param string $replacements ...
     *
     * @return string
     */
    public function cache_key ( $key ) {

        $arguments = func_get_args();
        $replacements = array_splice( $arguments, 1 );
        foreach ( $replacements as $i => $replacement ) {
            $needle = $i + 1;
            $key = str_replace( "{@$needle}", $replacement, $key );
        }

        return $key;
    }

    /**
     * @param mixed  $data
     * @param string $cache_id
     * @param int    $lifetime
     * @param int    $priority
     * @param array  $tags
     *
     * @return bool
     */
    public function set ( $data, $cache_id, $lifetime = NULL, $priority = 1, $tags = NULL ) {

        $automatic_serialization = $this->cache()->getOption( 'automatic_serialization' );
        $this->cache()->setOption( 'automatic_serialization', FALSE );
        $ok = $this->cache()->save( $this->encode( $data ),
                                    $this->id( $cache_id ),
                                    is_null( $tags ) ? $this->_tags : $tags,
                                    $lifetime,
                                    $priority );
        $this->cache()->setOption( 'automatic_serialization', $automatic_serialization );

        return $ok;
    }

    /**
     * @return Zend_Cache_Core
     */
    private function cache () {

        if ( is_null( $this->_cache ) ) {
            $this->_cache = Mage::app()->getCache();
        }

        return $this->_cache;
    }

    /**
     * @param $data
     *
     * @return string
     * @throws Mage_Core_Exception
     */
    private function encode ( $data ) {

        $encoded = NULL;
        if ( is_string( $data ) || is_numeric( $data ) ) {
            $encoded = self::TYPE_SIMPLE . $data;
        } elseif ( $data instanceof Varien_Object ) {
            $encoded = self::TYPE_VARIENOBJECT . json_encode( $data->getData() );
        } elseif ( is_bool( $data ) ) {
            $encoded = self::TYPE_BOOLEAN . ( $data == TRUE ? 1 : 0 );
        } elseif ( is_array( $data ) ) {
            $encoded = self::TYPE_ARRAY . json_encode( $data );
        } elseif ( is_object( $data ) ) {
            $encoded = self::TYPE_OBJECT . serialize( $data );
        } else {
            Mage::throwException( 'Appetite_Linio_Helper_Cache::encode --- ' . gettype( $data ) . ' is not supported.' );
        }

        return $encoded;
    }

    /**
     * @param string $cache_id
     *
     * @return string
     * @throws Mage_Core_Exception
     */
    private function id ( $cache_id ) {

        if ( !is_string( $cache_id ) || strlen( $cache_id ) == 0 ) {
            Mage::throwException( 'Appetite_Linio_Helper_Cache::id --- $cache_id must be a valid strin.' );
        }

        return "{$this->_prefix}:$cache_id";
    }

    /**
     * @param string $cache_id
     *
     * @return array|mixed|string|Varien_Object|null
     */
    public function get ( $cache_id ) {

        $data = NULL;
        $raw = $this->cache()->load( $this->id( $cache_id ), FALSE, TRUE );
        if ( $raw ) {
            $data = $this->decode( $raw );
        }

        return $data;
    }

    /**
     * @param string $raw
     *
     * @return mixed|array|string|Varien_Object
     * @throws Mage_Core_Exception
     */
    private function decode ( $raw ) {

        $type = substr( $raw, 0, 1 );
        $data = substr( $raw, 1, strlen( $raw ) - 1 );

        $decoded = NULL;
        switch ( $type ) {
            case self::TYPE_SIMPLE:
                $decoded = $data;
                break;
            case self::TYPE_VARIENOBJECT:
                $json = json_decode( $data, TRUE );
                $decoded = new Varien_Object();
                $decoded->setData( $json );
                break;
            case self::TYPE_BOOLEAN:
                $decoded = ( $data == '1' || $data == 1 )
                    ? TRUE
                    : ( ( $data == 0 || $data == '0' )
                        ? FALSE
                        : NULL );
                break;
            case self::TYPE_ARRAY:
                $decoded = json_decode( $data, TRUE );
                break;
            case self::TYPE_OBJECT:
                $decoded = unserialize( $data );
                break;
            default:
                Mage::throwException( 'Appetite_Linio_Helper_Cache::decode --- ' . $type . ' is not recognized.' );
                break;
        }

        return $decoded;
    }

    /**
     * @param string $cache_id
     *
     * @return bool
     */
    public function delete ( $cache_id ) {

        return $this->cache()->remove( $this->id( $cache_id ) );
    }

    /**
     * @param string $cache_id
     *
     * @return bool
     */
    public function exists ( $cache_id ) {

        return $this->cache()->test( $this->id( $cache_id ) );
    }
}
